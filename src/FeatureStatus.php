<?php

namespace Drupal\feature_toggle_helpers;

/**
 * A helper class for getting the status of a feature.
 */
class FeatureStatus {

  /**
   * Get the status for a feature flag.
   *
   * @param string $name
   *  The name of the feature flag.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public static function getStatus($name) {
    module_load_include('inc', 'feature_toggle', 'includes/feature_toggle.api');

    return feature_toggle_get_status($name);
  }

  /**
   * Determine if a particular feature flag is enabled.
   *
   * @param string $name
   *   The name of the feature flag.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public static function isEnabled($name) {
    return self::getStatus($name) === TRUE;
  }

}
