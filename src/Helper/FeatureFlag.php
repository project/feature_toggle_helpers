<?php

namespace Drupal\feature_toggle_helpers\Helper;

/**
 * A helper class for interacting with feature flags.
 */
class FeatureFlag {

  /**
   * Enable a feature flag.
   *
   * @param string $name
   *   The name of the feature flag.
   */
  public static function enable($name) {
    $statuses = variable_get('feature_flags_status');

    if (array_key_exists($name, $statuses)) {
      return;
    }

    $statuses[$name] = $name;
    variable_set('feature_flags_status', $statuses);

    module_invoke_all("feature_toggle_{$name}_enable");
  }

  /**
   * Get the value of a feature flag.
   *
   * @param string $name
   *  The name of the feature flag.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public static function getValue($name) {
    module_load_include('inc', 'feature_toggle', 'includes/feature_toggle.api');

    return feature_toggle_get_status($name);
  }

  /**
   * Determine if a particular feature flag is enabled.
   *
   * @param string $name
   *   The name of the feature flag.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public static function isEnabled($name) {
    return self::getValue($name) === TRUE;
  }

}
